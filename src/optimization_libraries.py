# Copyright (c) 2024 - Zizhe Wang
# https://zizhe.wang

#############################
#                           #
# INITIALIZATION ALGORITHMS #
#                           #
#############################

import importlib
import pkgutil
from pymoo.algorithms import moo
from pymoo.config import Config

Config.warnings['not_compiled'] = False

def initialize_algorithm(algorithm_name, pop_size=None):
    # Dynamically import all modules in pymoo.algorithms.moo
    algorithm_modules = {}
    for _, module_name, _ in pkgutil.iter_modules(moo.__path__):
        module = importlib.import_module(f"pymoo.algorithms.moo.{module_name}")
        for attribute_name in dir(module):
            attribute = getattr(module, attribute_name)
            if isinstance(attribute, type):  # Check if it's a class
                algorithm_modules[attribute_name.lower()] = attribute
    
    # Print the available algorithms for debugging
    # print("Available algorithms:", list(algorithm_modules.keys()))

    # Check if the algorithm name is in the imported modules
    if algorithm_name.lower() not in algorithm_modules:
        raise ValueError(f"Algorithm {algorithm_name} is not supported.")
    
    algorithm_class = algorithm_modules[algorithm_name.lower()]
    if pop_size:
        algorithm = algorithm_class(pop_size=pop_size)
    else:
        algorithm = algorithm_class()
    
    return algorithm