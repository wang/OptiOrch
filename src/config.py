# Copyright (c) 2024 - Zizhe Wang
# https://zizhe.wang

######################################
#                                    #
# GLOBAL SETTINGS AND CONFIGURATIONS #
#                                    #
######################################

import os
import json

# Load configuration from JSON file
CONFIG_FILE = 'config.json'
with open(CONFIG_FILE, 'r') as f:
    config = json.load(f)

# Assign configuration variables
MODEL_NAME = config['MODEL_NAME']
MODEL_FILE = config['MODEL_FILE']
SIMULATION_STOP_TIME = config['SIMULATION_STOP_TIME']  # in seconds
PRECISION = config['PRECISION']  # Results precision (decimal places)
PARAMETERS = config['PARAMETERS']  # Parameters to be varied
OBJECTIVES = config['OBJECTIVES']  # Objectives to be optimize
OBJECTIVE_NAMES = [obj['name'] for obj in OBJECTIVES]
MAXIMIZE = [obj['maximize'] for obj in OBJECTIVES]
PARAM_BOUNDS = config['PARAM_BOUNDS']  # Parameter range
OPTIMIZATION_CONFIG = config['OPTIMIZATION_CONFIG']
PLOT_CONFIG = config['PLOT_CONFIG']
N_JOBS = config['N_JOBS']
LIBRARY_CONFIG = config.get('LIBRARY_CONFIG', {})
LOAD_LIBRARIES = LIBRARY_CONFIG.get('LOAD_LIBRARIES', False)
LIBRARIES = LIBRARY_CONFIG.get('LIBRARIES', [])

# Derived configuration
BASE_DIR = os.getcwd()
MODEL_PATH = os.path.join(BASE_DIR, 'models', MODEL_FILE)

# Helper functions to get bounds and types
def get_bounds(param_bounds):
    return {param: bounds_info["bounds"] for param, bounds_info in param_bounds.items()}

def get_types(param_bounds):
    return {param: bounds_info["type"] for param, bounds_info in param_bounds.items()}

PARAM_BOUND_VALUES = get_bounds(PARAM_BOUNDS)
PARAM_TYPES = get_types(PARAM_BOUNDS)

# "N_JOBS": Parallel processing
    # Options: '-1', '1', 'n', 'None'
    # ====================================================================
    #   -1  = use all available CPU cores
    #    1  = disables parallel processing and runs the tasks sequentially
    #    n  = specifies the exact number of CPU cores to use
    #  None = will run the tasks sequentially, equivalent to '1'
    # ====================================================================