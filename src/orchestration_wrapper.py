# Copyright (c) 2024 - Zizhe Wang
# https://zizhe.wang

import os
import json
import shutil
from optimize_main import run_optimization
from OMPython import OMCSessionZMQ

class MOO4ModelicaWrapper:
    def __init__(self, orchestration_config_path, config_path):
        self.orchestration_config_path = orchestration_config_path
        self.config_path = config_path
        self.load_configs()
        self.backup_config_path = self.config_path + ".bak"
        self.backup_config_file()
        self.update_config_file()
        self.omc = OMCSessionZMQ()
        self.load_libraries()

    def load_configs(self):
        with open(self.orchestration_config_path, 'r') as f:
            self.orchestration_config = json.load(f)

        with open(self.config_path, 'r') as f:
            self.config = json.load(f)
    
    def backup_config_file(self):
        shutil.copy(self.config_path, self.backup_config_path)

    def restore_config_file(self):
        shutil.copy(self.backup_config_path, self.config_path)
        os.remove(self.backup_config_path)

    def update_config_file(self):
        # Update config with orchestration config
        self.config['MODEL_NAME'] = self.orchestration_config['MODEL_NAME']
        self.config['MODEL_FILE'] = self.orchestration_config['MODEL_FILE']
        self.config['SIMULATION_STOP_TIME'] = self.orchestration_config['SIMULATION_TIME']
        self.config['PARAMETERS'] = self.orchestration_config['TUNABLE_PARAMETERS']['PARAMETERS']
        self.config['PARAM_BOUNDS'] = self.orchestration_config['TUNABLE_PARAMETERS']['PARAM_BOUNDS']
        self.config['OBJECTIVES'] = self.orchestration_config['OBJECTIVES']
        self.config['PLOT_CONFIG'] = self.orchestration_config['PLOT_CONFIG']
        self.config['N_JOBS'] = self.orchestration_config['N_JOBS']
        self.config['LIBRARY_CONFIG'] = self.orchestration_config['LIBRARY_CONFIG']
        for key, value in self.orchestration_config['OPTIMIZATION_CONFIG'].items():
            self.config['OPTIMIZATION_CONFIG'][key] = value

        with open(self.config_path, 'w') as f:
            json.dump(self.config, f, indent=4)

    def update_config(self, simulation_inputs, simulation_time):
        # Update only the simulation-specific parameters
        self.config['SIMULATION_STOP_TIME'] = simulation_time
        for input_param, value in simulation_inputs.items():
            self.config[self.orchestration_config['INPUT_PARAMETERS'][input_param]] = value

        # Save the updated configuration to a file
        with open(self.config_path, 'w') as f:
            json.dump(self.config, f, indent=4)

    def load_libraries(self):
        if self.orchestration_config['LIBRARY_CONFIG']['LOAD_LIBRARIES']:
            for lib in self.orchestration_config['LIBRARY_CONFIG']['LIBRARIES']:
                library_path = os.path.join(os.getcwd(), 'models', lib['path']).replace('\\', '/')
                print(f"Loading library: {lib['name']} from {library_path}")
                load_library_result = self.omc.sendExpression(f'loadFile("{library_path}")')
                print(f"Load library result: {load_library_result}")
                if load_library_result is not True:
                    messages = self.omc.sendExpression("getErrorString()")
                    print(f"OpenModelica error messages: {messages}")
                    raise RuntimeError(f"Failed to load library: {lib['name']}")

    def run_optimization(self):
        run_optimization()

    def get_parameters(self):
        # Assuming the results are saved to a file "optimization_results.json"
        results_path = os.path.join(os.path.dirname(self.config_path), 'results', 'optimization_results.json')
        with open(results_path, 'r') as f:
            optimization_results = json.load(f)

        # The parameters are returned as they are in the results
        parameters = optimization_results["parameters"]

        return [
            {param: value for param, value in zip(self.config['PARAMETERS'], params)}
            for params in parameters
        ]

    def __del__(self):
        self.restore_config_file()