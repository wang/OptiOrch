# Copyright (c) 2024 - Zizhe Wang
# https://zizhe.wang

############################
#                          #
# MAIN OPTIMIZATION SCRIPT #
#                          #
############################

import os
import time
import json
import numpy as np
import matplotlib.pyplot as plt
from pymoo.core.problem import Problem
from pymoo.optimize import minimize
from optimization_libraries import initialize_algorithm
from parallel_computing import execute_parallel_tasks, cleanup_temp_dirs
from config import PARAMETERS, OBJECTIVE_NAMES, MAXIMIZE, PARAM_BOUND_VALUES, PARAM_TYPES, PRECISION, PLOT_CONFIG, OPTIMIZATION_CONFIG, N_JOBS

class OptimizationProblem(Problem):
    def __init__(self):
        self.param_names = list(PARAM_BOUND_VALUES.keys())
        self.param_types = [PARAM_TYPES[param] for param in self.param_names]
        self.objective_names = OBJECTIVE_NAMES
        self.maximize_indices = [i for i, maximize in enumerate(MAXIMIZE) if maximize]
        n_var = len(self.param_names)
        n_obj = len(self.objective_names)
        xl = np.array([PARAM_BOUND_VALUES[param][0] for param in self.param_names])
        xu = np.array([PARAM_BOUND_VALUES[param][1] for param in self.param_names])
        print(f"Number of variables: {n_var}")
        print(f"Lower bounds: {xl}")
        print(f"Upper bounds: {xu}")
        super().__init__(n_var=n_var, n_obj=n_obj, n_constr=0, xl=xl, xu=xu)

    def _evaluate(self, X, out, *args, **kwargs):
        for i, param_type in enumerate(self.param_types):
            if param_type == "int":
                X[:, i] = np.round(X[:, i]).astype(int)
            else:
                X[:, i] = X[:, i].astype(float)
        param_values_list = [dict(zip(self.param_names, x)) for x in X]
        results = execute_parallel_tasks(param_values_list)

        print(f"Initial results: {results}")
        print(f"Number of parameter sets evaluated: {len(param_values_list)}")
        print(f"Expected shape of results: ({len(param_values_list)}, {len(self.objective_names)})")

        if len(results) != len(param_values_list):
            missing_count = len(param_values_list) - len(results)
            results.extend([[np.nan] * len(self.objective_names)] * missing_count)

        for i in range(len(results)):
            for idx in self.maximize_indices:
                results[i] = list(results[i])
                results[i][idx] = -results[i][idx]

        print(f"Processed results: {results}")

        results_array = np.array(results)
        print(f"Shape of results array: {results_array.shape}")

        out["F"] = results_array.reshape(len(X), len(self.objective_names))
        out["X"] = X

def create_results_folder():
    results_folder = 'results'
    if not os.path.exists(results_folder):
        os.makedirs(results_folder)
    return results_folder

def run_optimization():
    results_folder = create_results_folder()
    pop_size = OPTIMIZATION_CONFIG['POP_SIZE']
    algorithm = initialize_algorithm(OPTIMIZATION_CONFIG['ALGORITHM_NAME'], pop_size)
    problem = OptimizationProblem()
    start_time = time.time()
    res = None

    try:
        for gen in range(OPTIMIZATION_CONFIG['N_GEN']):
            res = minimize(problem, algorithm, ("n_gen", 1), verbose=True)

    finally:
        cleanup_temp_dirs()
    end_time = time.time()
    elapsed_time = end_time - start_time
    print(f"Elapsed time: {elapsed_time:.2f} seconds")

    if res is not None:
        print_and_plot_results(res, problem)

        results_to_save = res.F.copy()
        parameters_to_save = res.X.copy()
        for i in range(len(results_to_save)):
            for idx in problem.maximize_indices:
                results_to_save[i][idx] = -results_to_save[i][idx]

        results_data = {
            "results": results_to_save.tolist(),
            "parameters": parameters_to_save.tolist(),
            "elapsed_time": elapsed_time,
        }
        filename = os.path.join(results_folder, f'optimization_results.json')
        with open(filename, 'w') as f:
            json.dump(results_data, f)
        print(f"Results stored in: {filename}")

    return res.F if res is not None else None, elapsed_time

def print_and_plot_results(res, problem):
    print("Optimization Results:")
    for i, result in enumerate(res.F):
        result = list(result)
        for idx in problem.maximize_indices:
            result[idx] = -result[idx]
        result = tuple(result)
        
        print(f"Solution {i}: ", end="")
        for name, value in zip(OBJECTIVE_NAMES, result):
            print(f"{name.capitalize()} = {value:.{PRECISION}f}", end=", ")
        print()

    if PLOT_CONFIG.get("ENABLE_PLOT", True):
        try: 
            for idx in problem.maximize_indices:
                res.F[:, idx] = -res.F[:, idx]
        
            plt.figure(figsize=(8, 6))
            plt.scatter(res.F[:, 0], res.F[:, 1])
            plt.xlabel(PLOT_CONFIG["PLOT_X"], fontsize=14)
            plt.ylabel(PLOT_CONFIG["PLOT_Y"], fontsize=14)
            plt.title(PLOT_CONFIG["PLOT_TITLE"], fontsize=16)
            plt.grid(True)
            plt.tight_layout()
            plt.show()
        except Exception as e:
            print(f"Error during plotting: {e}")

def main():
    print(f"Running optimization...")
    results, elapsed_time = run_optimization()
    print(f"Time: {elapsed_time:.2f} seconds")

if __name__ == "__main__":
    main()