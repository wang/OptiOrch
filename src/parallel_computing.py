# Copyright (c) 2024 - Zizhe Wang
# https://zizhe.wang

######################
#                    #
# PARALLEL COMPUTING #
#                    #
######################

import os
import shutil
import tempfile
import numpy as np
from time import sleep
from joblib import Parallel, delayed
from OMPython import OMCSessionZMQ
from config import MODEL_FILE, MODEL_NAME, SIMULATION_STOP_TIME, PARAMETERS, OBJECTIVE_NAMES, PARAM_BOUNDS, PARAM_TYPES, MODEL_PATH, PRECISION, N_JOBS, LOAD_LIBRARIES, LIBRARIES

temp_dirs = []  # List to store paths of temporary directories

def load_libraries(omc):
    """
    Function to load Modelica libraries.
    """
    for library in LIBRARIES:
        library_path = os.path.join(os.getcwd(), 'models', library['path']).replace('\\', '/')
        load_library_result = omc.sendExpression(f'loadFile("{library_path}")')
        print(f"Load library result: {load_library_result}")
        if load_library_result is not True:
            messages = omc.sendExpression("getErrorString()")
            print(f"OpenModelica error messages: {messages}")
            raise RuntimeError(f"Failed to load library: {library['name']}")

def optimization_function(param_values, retries=3, delay=2):
    """
    Function to optimize the Modelica model given specific parameter values.
    This function runs the simulation and retrieves the results.
    """
    temp_dir = tempfile.mkdtemp()  # Create a unique temporary directory for each worker
    temp_dirs.append(temp_dir)  # Store the path for later cleanup
    temp_dir = temp_dir.replace('\\', '/')

    for attempt in range(retries):
        try:
            omc = OMCSessionZMQ()  # Create a new OpenModelica session
            omc.sendExpression(f'cd("{temp_dir}")')

            # Copy model file to temporary directory
            temp_model_path = os.path.join(temp_dir, MODEL_FILE).replace('\\', '/')
            shutil.copy(MODEL_PATH, temp_model_path)

            if LOAD_LIBRARIES:
                load_libraries(omc)

            # Load the model in the worker session
            load_temp_model_result = omc.sendExpression(f"loadFile(\"{temp_model_path}\")")
            print(f"Load model result in worker: {load_temp_model_result}")
            if load_temp_model_result is not True:
                messages = omc.sendExpression("getErrorString()")
                print(f"OpenModelica error messages: {messages}")
                raise RuntimeError(f"Failed to load model: {MODEL_NAME}")

            # Build the model in the worker session
            build_model_result = omc.sendExpression(f"buildModel({MODEL_NAME})")
            if not (isinstance(build_model_result, tuple) and build_model_result[0]):
                messages = omc.sendExpression("getErrorString()")
                print(f"OpenModelica error messages: {messages}")
                raise RuntimeError(f"Failed to build model: {MODEL_NAME}")

            # Check if model files are created
            expected_files = [
                os.path.join(temp_dir, f"{MODEL_NAME}.exe").replace('\\', '/'),
                os.path.join(temp_dir, f"{MODEL_NAME}_init.xml").replace('\\', '/')
            ]
            for file in expected_files:
                if not os.path.isfile(file):
                    raise RuntimeError(f"Expected file not found: {file}")
            
            # Ensure param_values is a dictionary
            if isinstance(param_values, np.ndarray):
                param_values = {param: value for param, value in zip(PARAMETERS, param_values)}

            # Set model parameters
            rounded_param_values = {param: round(value, PRECISION) if PARAM_TYPES[param] == 'float' else int(value) for param, value in param_values.items()}
            for param, value in rounded_param_values.items():
                set_param_result = omc.sendExpression(f"setParameterValue({MODEL_NAME}, {param}, {value})")
                if not set_param_result:
                    raise RuntimeError(f"Failed to set model parameter: {param} = {value}")
            print(f"Parameters set: {rounded_param_values}")
            
            # Simulate the model
            simulate_result = omc.sendExpression(f"simulate({MODEL_NAME}, stopTime={SIMULATION_STOP_TIME})")
            # Retrieve simulation results
            result_values = {}
            for objective_name in OBJECTIVE_NAMES:
                value_command = f"val({objective_name}, {SIMULATION_STOP_TIME})"
                value = omc.sendExpression(value_command)
                print(f"Value for {objective_name} at {SIMULATION_STOP_TIME}: {value}")
                if value is None:
                    raise ValueError(f"Simulation result returned None for {objective_name}")
                if not isinstance(value, (float, int)):
                    raise ValueError(f"Simulation result for {objective_name} is not in expected format (float or int)")
                result_values[objective_name] = round(value, PRECISION)
            
            print(f"Simulation results: {result_values}")
            return list(result_values.values())

        except Exception as e:
            print(f"Error during simulation (attempt {attempt + 1}): {e}")
            sleep(delay)
        finally:
            if 'omc' in locals():
                shutdown_omc(omc)
    
    # If all attempts fail, return NaNs
    return [np.nan] * len(OBJECTIVE_NAMES)

def shutdown_omc(omc):
    try:
        omc.sendExpression("quit()")
        omc.__del__()  # Ensure OMC instance is properly closed
        print("OMC session closed successfully.")
    except Exception as e:
        print(f"Error during OMC shutdown: {e}")

# Cleanup temporary directories at the end of the script execution
def cleanup_temp_dirs():
    for temp_dir in temp_dirs:
        attempt = 0
        while True:
            try:
                shutil.rmtree(temp_dir)
                print(f"Successfully removed {temp_dir}")
                break  # Exit the loop if successful
            except PermissionError:
                attempt += 1
                print(f"PermissionError: Attempt {attempt} to remove {temp_dir}")
                sleep(2 ** attempt)  # Incremental backoff: 2, 4, 8, 16, ... seconds
            except Exception as e:
                print(f"Error: {e}")
                break  # Exit the loop for non-permission errors

def execute_parallel_tasks(tasks):
    results = Parallel(n_jobs=N_JOBS)(delayed(optimization_function)(task) for task in tasks)

    # Ensure results length matches tasks length by handling exceptions
    completed_results = [result for result in results if result is not None]

    # Debugging output
    print(f"Initial tasks: {len(tasks)}")
    print(f"Results: {len(completed_results)}")
    print(f"Results content: {completed_results}")

    return completed_results