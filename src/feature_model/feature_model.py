# Configuration
model_name = 'SimpleHeatingSystem.mo'
output_file = 'feature_model.json'
include_equations = True  # set to "False" if equations should be excluded

from anytree import Node, RenderTree
import json
from parse_modelica import parse_model

# Class to represent and manipulate the feature model tree
class FeatureModel:
    def __init__(self, root_name):
        self.root = Node(root_name)  # Initialize the root node of the tree

    # Method to add a component and its parameters to the tree
    def add_component(self, component_type, component_name, parameters):
        # Create a node for the component
        component_node = Node(f"{component_type} ({component_name})", parent=self.root)
        # Create nodes for each parameter and its value
        for param_name, param_value in parameters.items():
            Node(f"{param_name}={param_value}", parent=component_node)

    # Method to add an equation to the tree
    def add_equation(self, equation, index):
        Node(f"Equation {index+1}: {equation}", parent=self.root)

    # Method to display the tree structure in the console
    def display(self):
        for pre, fill, node in RenderTree(self.root):
            print("%s%s" % (pre, node.name))

    # Method to convert the tree to a dictionary for JSON serialization
    def to_dict(self, node=None):
        if node is None:
            node = self.root
        result = {"name": node.name, "children": [self.to_dict(child) for child in node.children]}
        return result

    # Method to save the tree to a JSON file
    def save_to_json(self, file_path):
        with open(file_path, 'w') as file:
            json.dump(self.to_dict(), file, indent=4)

if __name__ == "__main__":
    # Parse the Modelica file and extract components
    components, equations = parse_model(model_name, include_equations)

    # Create a feature model and add the extracted components
    feature_model = FeatureModel(model_name.split('.')[0])
    for component_type, component_name, parameters in components:
        feature_model.add_component(component_type, component_name, parameters)

    if include_equations:
        for index, equation in enumerate(equations):
            feature_model.add_equation(equation, index)

    # Display and save the feature tree
    feature_model.display()
    feature_model.save_to_json(output_file)