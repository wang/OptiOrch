model SimpleHeatingSystem
  parameter Real Q_max = 5000 "Maximum heating power in watts";
  parameter Real T_set = 293.15 "Setpoint temperature in Kelvin";
  parameter Real efficiency = 0.9 "Heater efficiency";
  parameter Real hysteresis = 0.5 "Hysteresis for temperature control";
  parameter Real T_comfort = 293.15 "Comfortable room temperature in Kelvin";

  Real Q_heat "Heating power";
  Real T_room(start = 278.15) "Room temperature";
  Real cost(start=0) "Cost of heating";
  Real energy(start=0) "Energy consumed";
  Real comfort(start=0) "Thermal comfort";
  Boolean heater_on(start=false) "Heater status";

  constant Real c_p = 1005 "Specific heat capacity of air (J/(kg·K))";
  constant Real m_air = 50 "Mass of air in the room (kg)";
  Real accumulated_discomfort(start=0) "Accumulated thermal discomfort";

equation
  // Room temperature dynamics
  der(T_room) = (Q_heat * efficiency - (T_room - 273.15)) / (m_air * c_p);

  // Heater control logic with hysteresis
  heater_on = if T_room < T_set - hysteresis then true else if T_room > T_set + hysteresis then false else pre(heater_on);
  Q_heat = if heater_on then Q_max else 0;

  // Calculations for energy and cost
  der(energy) = Q_heat * efficiency;
  der(cost) = 0.1 * Q_heat * efficiency;

  // Calculations for accumulated discomfort (thermal discomfort over time)
  der(accumulated_discomfort) = abs(T_room - T_comfort);
  
  // Comfort calculation related to energy consumption
  der(comfort) = if heater_on then efficiency * Q_heat / (accumulated_discomfort + 1) else 0;

end SimpleHeatingSystem;