# Copyright (c) 2024 - Zizhe Wang
# https://zizhe.wang

import json
from orchestration_wrapper import MOO4ModelicaWrapper
from orchestration_configurator import adaptive_control_loop, read_data_from_file

def main():
    # Get paths from the orchestration configuration
    orchestration_config_path = 'orchestration_config.json'
    config_path = 'config.json'

    # Load the orchestration configuration
    with open(orchestration_config_path, 'r') as f:
        orchestration_config = json.load(f)

    # Read data from the specified data file
    data_file = orchestration_config['DATA_FILE_PATH']
    data = read_data_from_file(data_file)

    # Initialize the MOO4ModelicaWrapper with the path to the MOO4Modelica config and the orchestration configuration
    moo_wrapper = MOO4ModelicaWrapper(orchestration_config_path=orchestration_config_path, config_path=config_path)

    # Run the adaptive control loop with the loaded data and configuration
    adaptive_control_loop(data, moo_wrapper, orchestration_config)

if __name__ == "__main__":
    main()