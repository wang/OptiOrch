# Copyright (c) 2024 - Zizhe Wang
# https://zizhe.wang

import json
import os
import re
import shutil
import tempfile
from OMPython import OMCSessionZMQ

def init_omc_session(temp_dir, model_file, library_config):
    omc = OMCSessionZMQ()
    omc.sendExpression(f'cd("{temp_dir}")')
    if library_config['LOAD_LIBRARIES']:
        for lib in library_config['LIBRARIES']:
            library_path = os.path.join(os.getcwd(), 'models', lib['path']).replace('\\', '/')
            omc.sendExpression(f'loadFile("{library_path}")')
            omc.sendExpression(f'loadModel({lib["name"]})')
    load_result = omc.sendExpression(f'loadFile("{model_file}")')
    print(f"Load model result: {load_result}")
    return omc

def build_model(omc, model_name):
    build_result = omc.sendExpression(f'buildModel({model_name})')
    return build_result

def simulate_and_evaluate(parameters, simulation_time, simulation_inputs, orchestration_config):
    # General setup and configuration
    model_file = orchestration_config['MODEL_FILE']
    model_name = orchestration_config['MODEL_NAME']
    model_path = os.path.join(os.getcwd(), 'models', model_file)
    param_types = {param: details['type'] for param, details in orchestration_config['TUNABLE_PARAMETERS']['PARAM_BOUNDS'].items()}
    
    # Create a temporary directory to avoid GUID mismatch issues
    temp_dir = tempfile.mkdtemp()

    evaluation_parameters = {obj['name']: obj['name'] for obj in orchestration_config['OBJECTIVES']}
    input_parameters = orchestration_config['INPUT_PARAMETERS']
    goal_expression = orchestration_config['CRITERIA']['GOAL_EXPRESSION']

    try:
        omc = init_omc_session(temp_dir, model_path, orchestration_config['LIBRARY_CONFIG'])
        build_result = build_model(omc, model_name)
        print(f"Model build result: {build_result}")

        # Set model parameters
        for param, value in parameters.items():
            param_type = param_types[param]
            if param_type == "int":
                value = int(value)
            response = omc.sendExpression(f'setParameterValue({model_name}, {param}, {value})')        
            print(f"Set parameter {param} to {value}: {response}")
            if response != 'Ok':
                raise RuntimeError(f"Error setting parameter {param}: {response}")

        # Set input parameters
        for input_param, value in simulation_inputs.items():
            omc.sendExpression(f'setParameterValue({model_name}, {input_parameters[input_param]}, {value})')
            print(f"Set input parameter {input_param} to {value}: {response}")

        # Run simulation
        result = omc.sendExpression(f'simulate({model_name}, stopTime={simulation_time})')
        print(f"Simulation result: {result}")
        termination_message = result.get('messages', "")
        print(f"Simulation termination message: {termination_message}")

        # Determine depletion time if simulation terminates early
        match = re.search(r'Simulation call terminate\(\) at time ([\d\.]+)', termination_message)
        depletion_time = float(match.group(1)) if match else simulation_time

        # Collect evaluation results
        evaluation_results = {}
        for criterion, expression in evaluation_parameters.items():
            result_value = omc.sendExpression(f"val({expression}, {depletion_time})")
            evaluation_results[criterion] = result_value
            print(f"{criterion}: {result_value}")

        # Evaluate the goal expression dynamically
        local_vars = {"evaluation_results": evaluation_results, "simulation_inputs": simulation_inputs}
        goal_satisfied = all(eval(expression, {}, local_vars) for expression in goal_expression)
        print(f"Goal satisfied: {goal_satisfied}")

        return evaluation_results, goal_satisfied, parameters, depletion_time
    
    finally:
        shutil.rmtree(temp_dir)  # Clean up the temporary directory

def read_data_from_file(filename):
    data = []
    with open(filename, 'r') as file:
        for line in file:
            time_str, *values = line.split()
            time_value = int(time_str.split(':')[0])  # Extract time value from the string
            data.append((time_value, *map(int, values)))  # Combine time value with other data
    return data

def adaptive_control_loop(data, moo_wrapper, orchestration_config):
    start_time = orchestration_config['TIME_CONFIG']['START_TIME']
    end_time = orchestration_config['TIME_CONFIG']['END_TIME']
    simulation_time = orchestration_config['SIMULATION_TIME']
    input_keys = list(orchestration_config['INPUT_PARAMETERS'].keys())  # List of input parameter keys
    
    report = []
    for entry in data:
        time_value, *input_values = entry
        if start_time <= time_value < end_time:
            print(f"Processing {orchestration_config['TIME_CONFIG']['TIME_UNIT']} {time_value}")
            
            # Create a dictionary of inputs
            simulation_inputs = dict(zip(input_keys, input_values))  
            # Update the config for the optimization framework
            moo_wrapper.update_config(simulation_inputs, simulation_time)
            # Run the optimization using the wrapper
            moo_wrapper.run_optimization()
            # Get the list of best parameters from the optimization results
            parameter_list = moo_wrapper.get_parameters()
            # Try each parameter set in order until goal is satisfied or options are exhausted
            goal_satisfied = False

            for best_parameters in parameter_list:
                try:
                    evaluation_results, goal_satisfied, parameters, depletion_time = simulate_and_evaluate(
                        best_parameters, simulation_time, simulation_inputs, orchestration_config)
                except Exception as e:
                    print(f"Simulation error: {e}")
                    continue
                
                if goal_satisfied:
                    break
            
            # Generate the report for each hour
            if goal_satisfied:
                report.append(f"{orchestration_config['TIME_CONFIG']['TIME_UNIT'].capitalize()} {time_value}: Goal satisfid with configuration {parameters}. Simulation ran for {depletion_time} seconds out of {simulation_time} seconds.")
            else:
                report.append(f"{orchestration_config['TIME_CONFIG']['TIME_UNIT'].capitalize()} {time_value}: No sufficient configuration found. Simulation ran for {depletion_time} seconds out of {simulation_time} seconds.")
    
    # Print the final report
    print("\nFinal Report:")
    for line in report:
        print(line)