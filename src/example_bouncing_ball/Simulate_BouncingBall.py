# (c) Zizhe Wang, zizhe.wang@tu-dresden.de
# This script is used to simulate the Modelica model using OMPython (https://openmodelica.org/doc/OpenModelicaUsersGuide/latest/ompython.html#ompython-openmodelica-python-interface)
# The core functions are the following:
#   omc = OMCSessionZMQ() - create an OMCSessionZMQ object
#   omc.sendExpression("loadFile()") - load the corresponding Modelica model
#   omc.sendExpression("instantiateModel()") - instantiate the corresponding Modelica model
#   omc.sendExpression("simulate(BouncingBall)") - simulate the corresponding Modelica model
#   omc.sendExpression("plot()") - plot the corresponding Modelica model
# In real applications there could be errors in each step (that was how I experienced...), so debugging has been added to each step
# Run this script by "python Simulate_BouncingBall.py", when each step is successful, the plot diagram will show
# Have fun :)

import os
from OMPython import OMCSessionZMQ

# Initialize the OpenModelica session
omc = OMCSessionZMQ()

# Define the absolute path to the model file
current_directory = os.getcwd()
model_path = os.path.join(current_directory, "BouncingBall.mo")

# Check if the model file exists
if not os.path.exists(model_path):
    raise FileNotFoundError(f"Modelica model file not found: {model_path}")
else:
    print(f"Modelica model file found: {model_path}")

# Load the Modelica model
try:
    load_result = omc.sendExpression(f'loadFile("{model_path}")')
    print(f"Load result: {load_result}")
    if not load_result:
        raise RuntimeError(f"Failed to load Modelica file: {model_path}")
    print("Model loaded sucessfully")
except Exception as e:
    print(f"Error loading Modelica model: {e}")
    raise

# Instantiate the Modelica model
try:    
    instantiate_result = omc.sendExpression("instantiateModel(BouncingBall)")
    print(f"Instantiate result: {instantiate_result}")
    if not instantiate_result:
        raise RuntimeError("Failed to instantiate the Modelica model.")
    print("Model instantiated successfully")
except Exception as e:
    print(f"Error instantiating Modelica model: {e}")
    raise

# Compile the model
try:
    compile_result = omc.sendExpression("buildModel(BouncingBall)")
    print(f"Compile result: {compile_result}")
    if not compile_result:
        raise RuntimeError("Failed to compile the Modelica model.")
    print("Model compiled successfully")
except Exception as e:
    print(f"Error compiling Modelica model: {e}")
    raise

# Simulate the model
try:
    simulate_result = omc.sendExpression("simulate(BouncingBall, stopTime=5.0)")
    print(f"Simulate result: {simulate_result}")
    if not simulate_result:
        raise RuntimeError("Failed to simulate the Modelica model.")
    print("Simulation completed")
except Exception as e:
    print(f"Error during simulation: {e}")
    raise

# Plot the result
try:
    plot_result = omc.sendExpression("plot(h)")
    print(f"Plot result: {plot_result}")
    if not plot_result:
        raise RuntimeError("Failed to plot the result.")
    print("Plot completed")
except Exception as e:
    print(f"Error during ploting: {e}")
    raise